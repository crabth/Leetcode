//给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 的那 两个 整数，并返回它们的数组下标。 
//
// 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。 
//
// 你可以按任意顺序返回答案。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [2,7,11,15], target = 9
//输出：[0,1]
//解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
// 
//
// 示例 2： 
//
// 
//输入：nums = [3,2,4], target = 6
//输出：[1,2]
// 
//
// 示例 3： 
//
// 
//输入：nums = [3,3], target = 6
//输出：[0,1]
// 
//
// 
//
// 提示： 
//
// 
// 2 <= nums.length <= 103 
// -109 <= nums[i] <= 109 
// -109 <= target <= 109 
// 只会存在一个有效答案 
// 
// Related Topics 数组 哈希表 
// 👍 10398 👎 0


import java.util.Arrays;
import java.util.HashMap;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[] twoSum(int[] nums, int target) {
//        int[] reslut = new int[2];
//        int a, b, index;
//        for (int i = 0, len = nums.length; i < len; i++) {
//            a = nums[i];
//            b = target-a;
//            index = i;
//            for (int j = i+1; j < len; j++) {
//                if (nums[j]==b) {
//                    index = j;
//                    break;
//                }
//            }
//            System.out.println(index);
//            if (index > i) {
//                reslut[0] = i;
//                reslut[1] = index;
//                break;
//            }
//        }
//        return reslut;
        Map<Integer, Integer> valueMap = new HashMap<>();
        int a, b, index;
        for (int i = 0, len = nums.length; i < len; i++) {
            b = target - nums[i];
            if (valueMap.containsKey(b)) {
                return new int[]{valueMap.get(b), i};
            }
            valueMap.put(nums[i], i);
        }
        throw new IllegalArgumentException("No two sum solution");
    }
}
//leetcode submit region end(Prohibit modification and deletion)
