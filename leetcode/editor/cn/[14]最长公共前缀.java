//编写一个函数来查找字符串数组中的最长公共前缀。 
//
// 如果不存在公共前缀，返回空字符串 ""。 
//
// 示例 1: 
//
// 输入: ["flower","flow","flight"]
//输出: "fl"
// 
//
// 示例 2: 
//
// 输入: ["dog","racecar","car"]
//输出: ""
//解释: 输入不存在公共前缀。
// 
//
// 说明: 
//
// 所有输入只包含小写字母 a-z 。 
// Related Topics 字符串


//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public String longestCommonPrefix(String[] strs) {
        String pre = "";
        int num = 0;
        String temp = "";
        int maxLen = 1;
        while (num < maxLen) {
            pre = "";
            for (String str:strs){
                System.out.println(str);
                temp = str.substring(0, num+1);
                if (pre.equals("")) {
                    pre = temp;
                }
                if (str.length() > maxLen) {
                    maxLen = str.length();
                }
                System.out.println(pre);
                if (!pre.equals(temp)) {
                    if (num > 0) {
                        pre = str.substring(0, num);
                    } else {
                        pre = "";
                    }
                    maxLen = num;
                    break;
                }
            }
            num++;
        }
        return pre;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
